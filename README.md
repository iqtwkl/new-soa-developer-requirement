# Selamat Bergabung di Team SOA Infosys Solusi Terpadu

Beberapa hal yang harus diperhatikan oleh anda


Install tools dibawah ini

## Git Client 
    
A. Untuk Windows Laptop _download file_ disini [![](https://img.shields.io/badge/-git-white?style=flat&logo=git)](https://git-scm.com/download/win)

B. Untuk Linux Laptop
        
- Debian Family 

    <details>
    <summary>Buka terminal run command dibawah</summary>

    ```
    $ sudo apt update
    $ sudo apt install git

    ```

    </details>

    <details>
    <summary>Untuk memverifikasi run command dibawah</summary>

    ```
    $ git --version

    ```

    </details>   


- RPM Family

    <details>
    <summary>Buka terminal run command dibawah</summary>

    ```
    $ sudo yum update
    $ sudo yum install git

    ```

    </details>
    
    <details>
    <summary>Untuk memverifikasi run command dibawah</summary>

    ```
    $ git --version

    ```

    </details>  
        
C. Untuk Laptop Mackintosh

<details>
<summary>buka terminal run command dibawah untuk menginstall Homebrew jika anda belum  menginstall Homebrew</summary>

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

```

</details>
        
<details>
<summary>setelah terinstall Homebrew run command dibawah</summary>

```
$ brew install git

```

</details>  

## Postman

_download file_ disini [![](https://img.shields.io/badge/-postman-white?style=flat&logo=postman)](https://www.postman.com/downloads/)

## Docker 

A. Untuk Laptop Windows _download file_ disini [![](https://img.shields.io/badge/-docker-white?style=flat&logo=docker)](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe)

<details>
<summary>Windows Instalasi</summary>

<p>
<div class="styles__scaleImages___3IpJT"><h3>Docker Desktop for Windows</h3>
<h3>Install</h3>
<p>Double-click <code>Docker for Windows Installer</code> to run the installer.</p>
<p>When the installation finishes, Docker starts automatically. The whale <img src="https://d1q6f0aelx0por.cloudfront.net/icons/whale-x-win.png"> in the notification area indicates that Docker is running, and accessible from a terminal.</p>
<h3>Run</h3>
<p>Open a command-line terminal like PowerShell, and try out some Docker commands!</p>
<p>Run <code>docker version</code> to check the version.</p>
<p>Run <code>docker run hello-world</code> to verify that Docker can pull and run images.</p>
<h3>Enjoy</h3>
<p>Docker is available in any terminal as long as the Docker Desktop for Windows app is running. Settings are available on the UI, accessible from the Docker whale in the taskbar.</p>
<p><img src="https://d1q6f0aelx0por.cloudfront.net/icons/d4win-artboard5.png" alt="Docker Desktop for Windows screen snaps"></p>
<h3>Documentation</h3>
<p>To learn more, read the <a href="https://docs.docker.com/docker-for-windows/">Docker Desktop for Windows
documentation</a>.</p>
<p>Be sure to check out <a href="https://docs.docker.com/docker-for-windows/#where-to-go-next">Where to go
next</a> for links
to labs and examples, and how to get started using swarm mode.</p>
</div>
</p>

</details>  

B. Untuk Laptop Mackintosh _download file_ disini [![](https://img.shields.io/badge/-docker-white?style=flat&logo=docker)](https://desktop.docker.com/mac/stable/Docker.dmg)

<details>
<summary>Mac Instalasi</summary>

<p>
<div data-testid="markdownContent" class="dMarkdown"><div class="styles__scaleImages___3IpJT"><h3>Docker Desktop for Mac</h3>
<h2>Installation</h2>
<h3>Main system requirements</h3>
<ul>
<li>Docker Desktop - macOS must be version 10.14 or newer, i.e. Mojave (10.14) or Catalina (10.15).</li>
<li>Mac hardware must be a 2010 or a newer model.</li>
<li>View all macOS system requirements <a href="https://docs.docker.com/docker-for-mac/install/#system-requirements">here</a>.</li>
</ul>
<h3>Install it</h3>
<p>Double-click <code>Docker.dmg</code> to start the install process.</p>
<p>When the installation completes and Docker starts, the whale in the top status bar shows that Docker is running, and accessible from a terminal.</p>
<p><img src="https://d1q6f0aelx0por.cloudfront.net/icons/whale-in-menu-bar.png" alt="Whale in menu bar"></p>
<h3>Run it</h3>
<p>Open a command-line terminal, and try out some Docker commands.</p>
<ul>
<li><p>Run <code>docker version</code> to check that you have the latest release installed.</p>
</li>
<li><p>Run <code>docker run hello-world</code> to verify that Docker is pulling images and running as expected.</p>
</li>
</ul>
<h3>Enjoy it</h3>
<p>Docker Desktop - Mac plays nicely on the desktop and command-line. You get the full Docker toolset, with many options configurable through the UI.</p>
<p><img src="https://d1q6f0aelx0por.cloudfront.net/icons/d4mac-artboard2.png" alt="Docker Desktop - Mac screen snaps"></p>
<h3>Documentation</h3>
<p>To learn more, read the <a href="https://docs.docker.com/docker-for-mac/">Docker Desktop for Mac
documentation</a>.</p>
<p>Be sure to check out <a href="https://docs.docker.com/docker-for-mac/#where-to-go-next">Where to go
next</a> for links
to labs and examples, and how to get started using swarm mode.</p>
</div></div>
</p>

</details>

## Integration Server

<details>
<summary>Install Docker IS</summary>

diharapkan sudah memiliki akun docker hub, dan sudah clone repository ini

- buka https://hub.docker.com/_/softwareag-webmethods-microservicesruntime , lakukan checkout
- pada folder repository ini pindah ke folder wm integration server
```
$ cd wm-integration-server
```
- kemudian lakukan docker compose up
```
$ docker-compose up
```
- tunggu hingga installasi dan semua package terload, kemudian integration server dapat di akses di port 5000, http://localhost:5000


</details>

## API Gateway

<details>
<summary>Install Docker APIGW</summary>

diharapkan sudah memiliki akun docker hub, dan sudah clone repository ini

- buka https://hub.docker.com/_/softwareag-apigateway, lakukan checkout
- pada folder repository ini pindah ke folder api gateway
```
$ cd api-gateway
```
- kemudian lakukan docker compose up
```
$ docker-compose up
```
- tunggu hingga installasi dan semua package terload, kemudian integration server dapat di akses di port 5000, http://localhost:9024

- cek log jika tidak bisa dibuka (log apigateway dan elasticsearch), FAQ:

    - error : Elasticsearch: Max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144], cek ini : https://stackoverflow.com/questions/51445846/elasticsearch-max-virtual-memory-areas-vm-max-map-count-65530-is-too-low-inc


</details>

## Python (Opsional)

<details>
<summary>Instalasi Python</summary>

Pastikan Python sudah terinstall di device anda, dengan cara buka cmd / terminal kemudian ketikkan:

```
$ py
```

atau

```
$ python
```

atau

```
$ python3
```

jika muncul versi python (seperti contoh dibawah ini) di command line anda maka python sudah terinstall (ketikkan exit() untuk keluar)

```
Python 3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

jika tidak maka install Python di device anda

<details>
<summary>Windows</summary>

- download Python -> [download python untuk windows](https://www.python.org/downloads/windows/)
- pastikan python sudah terdaftar di environment variable

</details>

<details>
<summary>Mac OS</summary>

[download python untuk Mac](https://www.python.org/downloads/mac-osx/)

</details>

<details>
<summary>Linux</summary>

For Red Hat, CentOS or Fedora, install the python3 and python3-devel packages.

For Debian or Ubuntu, install the python3.x and python3.x-dev packages.

For Gentoo, install the '=python-3.x*' ebuild (you may have to unmask it first).

</details>

Pastikan Pip sudah terinstall dengan Buka ulang cmd / terminal kemudian ketikkan

```
$ pip -V
```

atau

```
$ pip3 -V
```

jika muncul versi pip (seperti contoh dibawah) maka pip sudah terinstall

```
pip 20.2.4 from path\Python\Python37\site-packages\pip (python 3.7)
```

jika belum maka

<details>
<summary>Install pip</summary>

1. Donwload [get-pip.py](https://bootstrap.pypa.io/get-pip.py)
2. kemudian buka cmd / terminal lalu ketikkan

```
$ python get-pip.py
```

3. buka cmd / terminal, cek pip sudah terinstall atau belum

</details>

</details>

<details>
<summary>Install Docker Python</summary>

> TODO: bikin docker python agar local server environment sama dengan server development client. Apa ini harusnya di README.md nya setiap project ya?

> **_nanti nanya Kang_**

</details>

<details>
<summary>Project yang Menggunakan Python</summary>

1. [ncxtracking](https://gitlab.com/soa-ist/telkom-monitoring)

</details>

## PHP (Opsional)

<details>
<summary>Instalasi PHP</summary>

<details>
<summary>Windows</summary>

1. Install XAMPP

2. Install Composer

</details>

<details>
<summary>Linux</summary>

1. Install PHP

2. Install Composer

</details>

<details>
<summary>Mac OS</summary>

1. Install PHP

2. Install Composer

</details>

</details>

<details>
<summary>Install Docker PHP</summary>

> TODO: bikin docker PHP agar local server environment sama dengan server development client. Apa ini harusnya di README.md nya setiap project ya?

> **_nanti nanya Kang_**

</details>

<details>
<summary>Project yang Menggunakan PHP</summary>

1. [T-Cops](https://gitlab.com/soa-ist/t-cops)

</details>

#       
[![](https://img.shields.io/badge/-gitlab-lightgrey?style=flat&logo=gitlab)](https://gitlab.com/wastapun.derry) [![](https://img.shields.io/badge/-linkedIn-blue?style=flat&logo=linkedin)](https://www.linkedin.com/company/pt-infosys-solusi-terpadu/)

